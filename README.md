# Currency converter backend

Application goal is to make a simple currency conversion web backend. Application calls Open Exchange Rates API to get current currency converts and calculate convert any money value from one currency to another. Application also creates requests statistics stored in mongo DB.

## Usage
``` bash
# run application in development mode
$ npm run dev

# build application into ./dist directory
$ npm run build

# run application from build package
$ npm run start

# run all tests
$ npm run test
```
