export interface convertConfig {
    from: string;
    to: string;
    amount: number;
}

export interface convertResponse {
    from: string;
    to: string;
    amount: number;
    convertedAmount: number;
}

export interface openExchangeConvertResponse {
    destination: number;
    usd: number;
}
