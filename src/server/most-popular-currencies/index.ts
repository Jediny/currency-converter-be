import { Request, Response } from 'express';
import getCurrenciesCount from './get-currencies-count';

/**
 * @description Function handles express request
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
export default function mostPopularCurrencies(
    _req: Request,
    res: Response
): void {
    getCurrenciesCount((err, stats) => {
        if (err) {
            res.status(400).json({ err });
        } else {
            res.json(stats);
        }
    });
}
