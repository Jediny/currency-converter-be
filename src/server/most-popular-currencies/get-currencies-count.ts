import { connect } from '../lib/mongo';

/**
 * @description Function returns converted count grouped by destination currency
 * @export
 * @param {((err: Error | undefined, res: undefined | any) => void)} cb
 */
export default function getCurrenciesCount(
    cb: (err: Error | undefined, res: undefined | any) => void
): void {
    connect((err, mongoClient, db) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!mongoClient || !db) {
            return cb(new Error("Can't connect to DB"), undefined);
        }
        db.collection('convert_history')
            .aggregate([
                { $group: { _id: '$to', count: { $sum: 1 } } },
                { $sort: { count: -1 } },
                { $project: { _id: 0, currency: '$_id', count: 1 } }
            ])
            .toArray((err, result) => {
                mongoClient.close();
                if (err) {
                    return cb(err, undefined);
                }
                cb(undefined, result);
            });
    });
}
