require('dotenv').config();
import * as supertest from 'supertest';
import server from '../index';

describe('GET /most-popular-count', () => {
    it('Response code 200', (cb) => {
        supertest(server)
            .get('/most-popular-count')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!Array.isArray(res.body)) {
                    throw new Error('Response is not array');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
