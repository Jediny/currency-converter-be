require('dotenv').config();
import * as supertest from 'supertest';
import server from '../index';

describe('GET /converts-count', () => {
    it('Response code 200', (cb) => {
        supertest(server)
            .get('/converts-count')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!('count' in res.body)) {
                    throw new Error('Key "count" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
