import { connect } from '../lib/mongo';

/**
 * @description Function returns count of convert requests
 * @export
 * @param {((err: Error | undefined, res: undefined | number) => void)} cb
 */
export default function getConvertsCountStats(
    cb: (err: Error | undefined, res: undefined | number) => void
): void {
    connect((err, mongoClient, db) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!mongoClient || !db) {
            return cb(new Error("Can't connect to DB"), undefined);
        }
        db.collection('convert_history').countDocuments(
            {},
            {},
            (err, count) => {
                mongoClient.close();
                if (err) {
                    return cb(err, undefined);
                }
                cb(undefined, count);
            }
        );
    });
}
