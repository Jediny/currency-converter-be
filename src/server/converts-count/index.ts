import { Request, Response } from 'express';
import getConvertsCountStats from './get-converts-count-stats';

/**
 * @description Function handles express request
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
export default function mostPopularCurrencies(
    _req: Request,
    res: Response
): void {
    getConvertsCountStats((err, count) => {
        if (err) {
            res.status(400).json({ err });
        } else {
            res.json({ count });
        }
    });
}
