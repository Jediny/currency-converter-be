require('dotenv').config();
import * as supertest from 'supertest';
import server from '../index';

describe('GET /usd-amount', () => {
    it('Response code 200', (cb) => {
        supertest(server)
            .get('/usd-amount')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!('sum' in res.body)) {
                    throw new Error('Key "sum" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
