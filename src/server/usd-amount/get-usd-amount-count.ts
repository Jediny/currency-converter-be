import { connect } from '../lib/mongo';

/**
 * @description Function returns sum converted amount in USD
 * @export
 * @param {((err: Error | undefined, res: undefined | number) => void)} cb
 */
export default function getUsdAmountCount(
    cb: (err: Error | undefined, res: undefined | number) => void
): void {
    connect((err, mongoClient, db) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!mongoClient || !db) {
            return cb(new Error("Can't connect to DB"), undefined);
        }
        db.collection('convert_history')
            .aggregate([
                { $match: { usd: { $exists: true } } },
                {
                    $group: {
                        _id: undefined,
                        sum: { $sum: '$usd' }
                    }
                }
            ])
            .toArray((err, result: any) => {
                mongoClient.close();
                if (err) {
                    return cb(err, undefined);
                }
                cb(undefined, result.length ? result[0].sum : 0);
            });
    });
}
