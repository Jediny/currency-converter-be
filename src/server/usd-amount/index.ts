import { Request, Response } from 'express';
import getUsdAmountCount from './get-usd-amount-count';

/**
 * @description Function handles express request
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
export default function usdAmount(_req: Request, res: Response): void {
    getUsdAmountCount((err, sum) => {
        if (err) {
            res.status(400).json({ err });
        } else {
            res.json({ sum });
        }
    });
}
