import { Router } from 'express';
import converter from './converter';
import currenciesList from './currencies-list';
import mostPopularCurrencies from './most-popular-currencies';
import convertsCount from './converts-count';
import usdAmount from './usd-amount';

const router = Router();

router.get('/converter', converter);
router.get('/currencies-list', currenciesList);
router.get('/most-popular-count', mostPopularCurrencies);
router.get('/converts-count', convertsCount);
router.get('/usd-amount', usdAmount);

export default router;
