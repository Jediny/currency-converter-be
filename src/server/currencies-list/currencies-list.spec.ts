require('dotenv').config();
import * as supertest from 'supertest';
import server from './../index';

describe('GET /currencies-list', () => {
    it('Response code 200', (cb) => {
        supertest(server)
            .get('/currencies-list')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (Object.keys(res.body).length === 0) {
                    throw new Error('Empty currencies list');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
