import { Request, Response } from 'express';
import getCurrencies from '../lib/open-exchange-rates/get-currencies';

/**
 * @description Function handles express request
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
export default function currenciesList(_req: Request, res: Response) {
    getCurrencies((err, list) => {
        if (err) {
            res.status(400).json({ err: err.message });
        } else {
            res.json(list);
        }
    });
}
