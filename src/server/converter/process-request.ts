import * as async from 'async';
import {
    convertConfig,
    convertResponse,
    openExchangeConvertResponse
} from '../../types';
import saveConvert from './save-convert';
import convert from './convert';

/**
 * @description Function handles request - calculate convert and save result into DB
 * @export
 * @param {convertConfig} convertConfig
 * @param {((err: Error | undefined, res: undefined | convertResponse) => void)} cb
 */
export default function processRequest(
    convertConfig: convertConfig,
    cb: (err: Error | undefined, res: undefined | convertResponse) => void
): void {
    async.waterfall(
        [
            (
                callback: (
                    err: Error | undefined,
                    res: undefined | openExchangeConvertResponse
                ) => void
            ) => {
                convert(convertConfig, (err, res) => {
                    if (err) {
                        return callback(err, undefined);
                    }
                    callback(undefined, res);
                });
            },
            (
                amounts: openExchangeConvertResponse,
                callback: (
                    err: Error | undefined,
                    res: undefined | openExchangeConvertResponse
                ) => void
            ) => {
                saveConvert(convertConfig, amounts, (err) => {
                    callback(err, amounts);
                });
            }
        ],
        (err, amounts: openExchangeConvertResponse | any) => {
            if (err) {
                return cb(err, undefined);
            }
            cb(undefined, {
                ...convertConfig,
                convertedAmount: amounts.destination
            });
        }
    );
}
