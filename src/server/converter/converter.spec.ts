require('dotenv').config();
import * as supertest from 'supertest';
import server from './../index';

jest.mock('./save-convert');
jest.mock('../lib/open-exchange-rates/latest-usd-rates');

describe('GET /converter', () => {
    it('Response code 200', (cb) => {
        supertest(server)
            .get('/converter')
            .query({ from: 'czk', to: 'eur', amount: 1 })
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                expect(res.body).toMatchObject({
                    from: 'czk',
                    to: 'eur',
                    amount: 1,
                    convertedAmount: 0.5
                });
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });

    it('Response code 400', (cb) => {
        supertest(server)
            .get('/converter')
            .query({ from: 'asd', to: 'eur', amount: 1 })
            .expect(400)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                expect(res.body).toHaveProperty('err');
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });

    it('Response code 400', (cb) => {
        supertest(server)
            .get('/converter')
            .query({ from: 'czk', to: 'eur', amount: 'test' })
            .expect(400)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                expect(res.body).toHaveProperty('err');
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
