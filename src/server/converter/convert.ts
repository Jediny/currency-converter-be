import { convertConfig, openExchangeConvertResponse } from '../../types';
import getLatestUsdRates from '../lib/open-exchange-rates/get-latest-usd-rates';

/**
 * @description Calculate convert into destination currency and USD
 * @export
 * @param {convertConfig} convertConfig
 * @param {((
 *         err: Error | undefined,
 *         res: openExchangeConvertResponse | undefined
 *     ) => void)} cb
 */
export default function convert(
    convertConfig: convertConfig,
    cb: (
        err: Error | undefined,
        res: openExchangeConvertResponse | undefined
    ) => void
): void {
    getLatestUsdRates(convertConfig, (err, res) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!res) {
            return cb(new Error('Error in third part app'), undefined);
        }
        const fromUC = convertConfig.from.toUpperCase();
        const toUC = convertConfig.to.toUpperCase();
        if (!(fromUC in res)) {
            return cb(new Error(`Unknown currency "${fromUC}"`), undefined);
        }
        if (!(toUC in res)) {
            return cb(new Error(`Unknown currency "${toUC}"`), undefined);
        }
        const fromRate = res[fromUC];
        const toRate = res[toUC];

        cb(undefined, {
            destination: (toRate / fromRate) * convertConfig.amount,
            usd: (1 / fromRate) * convertConfig.amount
        });
    });
}
