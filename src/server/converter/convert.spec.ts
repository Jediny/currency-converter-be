import convert from './convert';

jest.mock('../lib/open-exchange-rates/latest-usd-rates');

describe('Tests for convert calculation', () => {
    it('success convert calculation', (cb) => {
        convert({ from: 'czk', to: 'eur', amount: 1 }, (err, res) => {
            expect(err).toBeUndefined();
            expect(res).toMatchObject({
                destination: 0.5,
                usd: 0.25
            });
            cb();
        });
    });

    it('unknown from currency', (cb) => {
        convert({ from: 'asd', to: 'eur', amount: 1 }, (err, res) => {
            expect(res).toBeUndefined();
            expect(err).toMatchObject(new Error('Unknown currency "ASD"'));
            cb();
        });
    });

    it('unknown from currency', (cb) => {
        convert({ from: 'czk', to: 'asd', amount: 1 }, (err, res) => {
            expect(res).toBeUndefined();
            expect(err).toMatchObject(new Error('Unknown currency "ASD"'));
            cb();
        });
    });
});
