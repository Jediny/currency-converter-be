/**
 * @description Function validates input request
 * @export
 * @param {{
 *     [key: string]: any;
 * }} query
 * @returns {string[]} If any error occurs
 */
export default function validateRequest(query: {
    [key: string]: any;
}): string[] {
    const errors: string[] = [];
    if (!('amount' in query)) {
        errors.push('Missing "amount" key in request query');
    }
    if (!('from' in query)) {
        errors.push('Missing "from" key in request query');
    }
    if (!('to' in query)) {
        errors.push('Missing "to" key in request query');
    }
    if (query.amount && isNaN(Number(query.amount))) {
        errors.push(`Amount "${query.amount}" is not number`);
    }
    return errors;
}
