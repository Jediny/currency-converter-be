import { convertConfig, openExchangeConvertResponse } from '../../types';
import { connect } from '../lib/mongo';

/**
 * @description Function saves convert result into mongo DB
 * @export
 * @param {convertConfig} convertConfig
 * @param {openExchangeConvertResponse} convertedAmounts
 * @param {((err: Error | undefined, res: undefined | any) => void)} cb
 */
export default function saveConvert(
    convertConfig: convertConfig,
    convertedAmounts: openExchangeConvertResponse,
    cb: (err: Error | undefined, res: undefined | any) => void
): void {
    connect((err, mongoClient, db) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!mongoClient || !db) {
            return cb(new Error("Can't connect to DB"), undefined);
        }
        db.collection('convert_history').insertOne(
            {
                ...convertConfig,
                ...convertedAmounts,
                ts: new Date()
            },
            (err, result) => {
                mongoClient.close();
                if (err) {
                    return cb(err, undefined);
                }
                cb(undefined, result.ops[0]);
            }
        );
    });
}
