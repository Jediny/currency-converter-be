import { convertConfig, openExchangeConvertResponse } from '../../../types';

export default function saveConvert(
    convertConfig: convertConfig,
    convertedAmounts: openExchangeConvertResponse,
    cb: (err: Error | undefined, res: undefined | any) => void
): void {
    cb(undefined, {
        ...convertConfig,
        ...convertedAmounts,
        ts: new Date()
    });
}
