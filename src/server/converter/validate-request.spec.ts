import validateRequest from './validate-request';

describe('Tests for request validation', () => {
    it('success validation', () => {
        const errors = validateRequest({ amount: 100, from: 'czk', to: 'eur' });
        expect(errors).toEqual([]);
    });

    it('success validation - float amount', () => {
        const errors = validateRequest({
            amount: '10.5',
            from: 'czk',
            to: 'eur'
        });
        expect(errors).toEqual([]);
    });

    it('success validation - float amount', () => {
        const errors = validateRequest({
            amount: 10.5,
            from: 'czk',
            to: 'eur'
        });
        expect(errors).toEqual([]);
    });

    it('validation fail - missing amount', () => {
        const errors = validateRequest({ from: 'czk', to: 'eur' });
        expect(errors).toEqual(['Missing "amount" key in request query']);
    });

    it('validation fail - missing from', () => {
        const errors = validateRequest({ amount: 10, to: 'eur' });
        expect(errors).toEqual(['Missing "from" key in request query']);
    });

    it('validation fail - missing to', () => {
        const errors = validateRequest({ amount: 10, from: 'eur' });
        expect(errors).toEqual(['Missing "to" key in request query']);
    });

    it('validation fail - string amount', () => {
        const errors = validateRequest({
            amount: 'test',
            from: 'czk',
            to: 'eur'
        });
        expect(errors).toEqual(['Amount "test" is not number']);
    });

    it('validation fail - amount comma delimiter', () => {
        const errors = validateRequest({
            amount: '10,5',
            from: 'czk',
            to: 'eur'
        });
        expect(errors).toEqual(['Amount "10,5" is not number']);
    });
});
