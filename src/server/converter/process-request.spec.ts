import processRequest from './process-request';

jest.mock('./save-convert');
jest.mock('../lib/open-exchange-rates/latest-usd-rates');

describe('Tests for request processing', () => {
    it('success request', (cb) => {
        processRequest({ from: 'czk', to: 'eur', amount: 1 }, (err, res) => {
            expect(err).toBeUndefined();
            expect(res).toMatchObject({
                from: 'czk',
                to: 'eur',
                amount: 1,
                convertedAmount: 0.5
            });
            cb();
        });
    });

    it('request fail', (cb) => {
        processRequest({ from: 'asd', to: 'eur', amount: 1 }, (err, res) => {
            expect(err).toBeDefined();
            expect(res).toBeUndefined();
            cb();
        });
    });
});
