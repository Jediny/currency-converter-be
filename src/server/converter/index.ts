import { Request, Response } from 'express';
import validateRequest from './validate-request';
import { convertConfig } from '../../types';
import processRequest from './process-request';

/**
 * @description Function handles express request
 * @export
 * @param {Request} req
 * @param {Response} res
 * @returns
 */
export default function converter(req: Request, res: Response) {
    const errors = validateRequest(req.query);
    if (errors.length) {
        return res.status(400).json({ err: errors });
    }
    const convertConfig: convertConfig = {
        from: req.query.from,
        to: req.query.to,
        amount: parseInt(req.query.amount)
    };
    processRequest(convertConfig, (err, convertResponse) => {
        if (err) {
            res.status(400).json({ err: err.message });
        } else {
            res.json(convertResponse);
        }
    });
}
