import { MongoClient, Db } from 'mongodb';

/**
 * @description Function to create mongo client and Db object
 * @export
 * @param {((
 *         err: Error | undefined,
 *         mongoClient: MongoClient | undefined,
 *         db: Db | undefined
 *     ) => void)} cb
 */
export function connect(
    cb: (
        err: Error | undefined,
        mongoClient: MongoClient | undefined,
        db: Db | undefined
    ) => void
) {
    if (!process.env.MOGNO_URI) {
        console.error('Missing env variable "MOGNO_URI"');
        return cb(new Error("Can't connect to DB"), undefined, undefined);
    }
    MongoClient.connect(
        process.env.MOGNO_URI,
        { useNewUrlParser: true },
        (err: Error, mongoClient: MongoClient) => {
            if (err) {
                cb(err, undefined, undefined);
            } else {
                const mongoDb = mongoClient.db(process.env.MOGNO_DB);
                cb(undefined, mongoClient, mongoDb);
            }
        }
    );
}
