import * as request from 'request';

/**
 * @description Function returns list of all currency symbols available from the Open Exchange Rates API
 * @export
 * @param {convertConfig} convertConfig
 * @param {((
 *         err: Error | undefined,
 *         res: { [key: string]: string } | undefined
 *     ) => void)} cb
 */
export default function getCurrencies(
    cb: (
        err: Error | undefined,
        res: { [key: string]: string } | undefined
    ) => void
): void {
    request(
        `${process.env.EXCHANGE_API_URL}/currencies.json`,
        {
            json: true
        },
        (err, _response, body) => {
            if (err) {
                return cb(err, undefined);
            }

            cb(undefined, body);
        }
    );
}
