import * as request from 'request';
import { convertConfig } from '../../../types';

/**
 * @description Function returns latest exchange rates available from the Open Exchange Rates API
 * @export
 * @param {convertConfig} convertConfig
 * @param {((
 *         err: Error | undefined,
 *         res: { [key: string]: number } | undefined
 *     ) => void)} cb
 */
export default function getLatestUsdRates(
    convertConfig: convertConfig,
    cb: (
        err: Error | undefined,
        res: { [key: string]: number } | undefined
    ) => void
): void {
    request(
        `${process.env.EXCHANGE_API_URL}/latest.json/`,
        {
            qs: {
                app_id: process.env.EXCHANGE_API_KEY,
                base: 'USD',
                symbols: `${convertConfig.from},${convertConfig.to}`
            },
            json: true
        },
        (err, _response, body) => {
            if (err) {
                return cb(err, undefined);
            }

            if (!('rates' in body)) {
                console.error(
                    'Missing "rates" in Open exchange response',
                    body
                );
                return cb(new Error('Error in third part app'), undefined);
            }

            cb(undefined, body.rates);
        }
    );
}
