import { convertConfig } from '../../../../types';

export default function latestUsdRates(
    _convertConfig: convertConfig,
    cb: (
        err: Error | undefined,
        res: { [key: string]: number } | undefined
    ) => void
): void {
    cb(undefined, { CZK: 4, EUR: 2 });
}
